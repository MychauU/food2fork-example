package io.dobrenko.food2forkexample.model.food2fork.jto.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Map;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class RecipeJTO {

    private Recipe recipe;
}
