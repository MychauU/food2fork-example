package io.dobrenko.food2forkexample.model.food2fork.jto.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;
import java.util.Map;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class Recipe {
    @JsonProperty("image_url")
    private String imageUrl;
    @JsonProperty("source_url")
    private String sourceUrl;
    @JsonProperty("f2f_url")
    private String f2FUrl;
    private String title;
    private String publisher;
    @JsonProperty("publisher_url")
    private String publisherUrl;
    @JsonProperty("social_rank")
    private Double socialRank;
    @JsonProperty("recipe_id")
    private String recipeId;
    private List<String> ingredients;

}
