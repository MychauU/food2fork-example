package io.dobrenko.food2forkexample.model.food2fork.jto.response;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;
@Data
@Accessors(chain = true)
public class SearchJTO {

    private int count;
    private List<SearchRecipe> recipes;
}
