package io.dobrenko.food2forkexample.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import butterknife.BindView;
import butterknife.OnClick;
import io.dobrenko.food2forkexample.R;
import io.dobrenko.food2forkexample.activity.food2fork.search.Food2ForkSearchActivity;

public class StartupActivity extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    private static final String TAG = "StartupActivity";
    private static Class<? extends Object> food2ForkSearchActivity = Food2ForkSearchActivity.class;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
    }

    @Override
    public int getLayoutFieldId() {
        return R.layout.startup_activity;
    }


    @OnClick(R.id.startup_continue_button)
    public void continueToActivity(){
        Intent intent = new Intent(this, food2ForkSearchActivity);
        startActivity(intent);
    }


}
