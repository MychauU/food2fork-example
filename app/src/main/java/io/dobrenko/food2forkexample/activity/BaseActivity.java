package io.dobrenko.food2forkexample.activity;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import butterknife.ButterKnife;
import butterknife.Unbinder;

public abstract class  BaseActivity  extends AppCompatActivity{

    private View contentView;
    private Unbinder butterKnifeUnbinder;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(inflateLayout());
        butterKnifeUnbinder = ButterKnife.bind(this);
    }

    private View inflateLayout() {
        contentView = getLayoutInflater().inflate(getLayoutFieldId(), null, false);
        ViewGroup root = (ViewGroup) contentView;
        return root;
    }

    @LayoutRes
    public abstract int getLayoutFieldId();

    @Override
    protected void onDestroy(){
        butterKnifeUnbinder.unbind();
        super.onDestroy();
    }

    protected void showToast(int r) {
        Toast.makeText(this, r, Toast.LENGTH_LONG).show();
    }

    protected void showToast(String r) {
        Toast.makeText(this, r, Toast.LENGTH_LONG).show();
    }
}
