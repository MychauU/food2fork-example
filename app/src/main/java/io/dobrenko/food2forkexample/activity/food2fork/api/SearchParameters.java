package io.dobrenko.food2forkexample.activity.food2fork.api;



public enum SearchParameters {

    QUERY("q"),
    PAGE("page"),
    SORT("sort");

    private final String value;

    private SearchParameters(String value) {
        this.value=value;
    }

    @Override
    public String toString(){
        return value;
    }
}
