package io.dobrenko.food2forkexample.activity.food2fork.search;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import io.dobrenko.food2forkexample.R;
import io.dobrenko.food2forkexample.model.food2fork.jto.response.SearchRecipe;

public class RecipesRecyclerViewAdapter extends RecyclerView.Adapter<RecipeListItemViewHolder> {

    private List<SearchRecipe> recipeList;
    private AdapterCallback adapterCallback;
    private Context context;

    @Override
    public RecipeListItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from( parent.getContext()).inflate(R.layout.search_recyclerview_item, parent, false);
        RecipeListItemViewHolder viewHolder = new RecipeListItemViewHolder(view);
        context = parent.getContext();
        adapterCallback = (AdapterCallback)context;
        viewHolder.getItemShowMoreButton().setOnClickListener(v -> {
            int pos = viewHolder.getAdapterPosition();
            if (pos != RecyclerView.NO_POSITION) {
                adapterCallback.onShowMoreButtonClick(recipeList.get(pos).getRecipeId());
            }
        });

        return  viewHolder;
    }

    @Override
    public void onBindViewHolder(RecipeListItemViewHolder holder, int position) {
        SearchRecipe recipe = recipeList.get(position);
        Double rank = recipe.getSocialRank();
        if (rank != null) {
            holder.getItemRankTextView().setText(String.format("%.2f", rank));
        }
        else {
            holder.getItemRankTextView().setText(R.string.unknown);
        }
        holder.getItemTitleTextView().setText(recipe.getTitle());

    }

    @Override
    public int getItemCount() {
        return recipeList.size();
    }

    public void setRecipeList(List<SearchRecipe> recipeList) {
        this.recipeList = recipeList;
        notifyDataSetChanged();
    }

    public interface AdapterCallback {
        void onShowMoreButtonClick(String recipeId);
    }
}
