package io.dobrenko.food2forkexample.activity.food2fork.api;

import java.util.Map;

import io.dobrenko.food2forkexample.model.food2fork.jto.response.RecipeJTO;
import io.dobrenko.food2forkexample.model.food2fork.jto.response.SearchJTO;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;



public interface Food2ForkApi {

    String api="a59a0a0de98cd443004ae6e24f44e81c";

    @GET("search?key="+api)
    Call<SearchJTO> search();

    @GET("search?key="+api)
    Call<SearchJTO> search(@QueryMap Map<String, String> options);

    @GET("get?key="+api)
    Call<RecipeJTO> recipe(@Query("rId") String recipeId);
}
