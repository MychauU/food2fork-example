package io.dobrenko.food2forkexample.activity.food2fork.api;


import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

public class Food2ForkApiBuilder {

    private static final String baseUrl = "http://food2fork.com/api/";


    public static Food2ForkApi createFood2ForkClient(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(JacksonConverterFactory.create())
                .build();
        return retrofit.create(Food2ForkApi.class);
    }



}
