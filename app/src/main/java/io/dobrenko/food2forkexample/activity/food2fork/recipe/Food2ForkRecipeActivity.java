package io.dobrenko.food2forkexample.activity.food2fork.recipe;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import butterknife.BindView;
import io.dobrenko.food2forkexample.R;
import io.dobrenko.food2forkexample.activity.BaseActivity;
import io.dobrenko.food2forkexample.activity.food2fork.api.Food2ForkApi;
import io.dobrenko.food2forkexample.activity.food2fork.api.Food2ForkApiBuilder;
import io.dobrenko.food2forkexample.model.food2fork.jto.response.Recipe;
import io.dobrenko.food2forkexample.model.food2fork.jto.response.RecipeJTO;
import io.dobrenko.food2forkexample.network.PhoneConnection;
import io.dobrenko.food2forkexample.utlis.DownloadImageTask;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Food2ForkRecipeActivity extends BaseActivity {

    public static final String RECIPE_ID = "recipe_id";
    public static final String TAG = "Food2ForkRecipeActivity";
    private String recipeId = null;
    private Food2ForkApi food2ForkClient;

    @BindView(R.id.recipe_imageView)
    ImageView imageView;
    @BindView(R.id.recipe_ingredients_listView)
    ListView listView;
    @BindView(R.id.recipe_food2forkUrl_textView)
    TextView food2ForkUrlTextView;
    @BindView(R.id.recipe_publisherUrl_textView)
    TextView publisherUrlTextView;
    @BindView(R.id.recipe_sourceUrl_textView)
    TextView sourceUrlTextView;
    @BindView(R.id.recipe_publisher_textView)
    TextView publisherTextView;
    @BindView(R.id.recipe_socialRank_textView)
    TextView socialRankTextView;
    @BindView(R.id.recipe_title_textView)
    TextView recipeTitleTextView;
    @BindView(R.id.recipe_scrollView)
    ScrollView scrollView;
    @BindView(R.id.relative_layout_progress_bar)
    RelativeLayout relativeLayoutWithProgressBar;
    @BindView(R.id.recipe_information_textView)
    TextView informationTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        food2ForkClient = Food2ForkApiBuilder.createFood2ForkClient();
        initialize();
    }

    private void initialize() {
        getDataFromAnotherActivity();
        if (recipeId != null) {
            getRecipeDataFromApi();
        } else {
            informationTextView.setText("Error occur");
            informationTextView.setVisibility(View.VISIBLE);
            throw new RuntimeException("No data was transferred to Food2ForkRecipeActivity in bundle");

        }
    }

    public void getRecipeDataFromApi() {
        relativeLayoutWithProgressBar.setVisibility(View.VISIBLE);
        scrollView.setVisibility(View.GONE);
        if (!PhoneConnection.isDeviceOnline(this)) {
            showToast("There is no internet connection");
            informationTextView.setText(R.string.no_connection);
            informationTextView.setVisibility(View.VISIBLE);

            return;
        }
        Call<RecipeJTO> call = food2ForkClient.recipe(recipeId);
        call.enqueue(new Callback<RecipeJTO>() {
            @Override
            public void onResponse(Call<RecipeJTO> call, Response<RecipeJTO> response) {
                relativeLayoutWithProgressBar.setVisibility(View.GONE);
                RecipeJTO body = response.body();
                if (body != null) {
                    loadDataFromRequest(body);
                    scrollView.setVisibility(View.VISIBLE);
                } else {
                    informationTextView.setVisibility(View.VISIBLE);
                    informationTextView.setText(R.string.information_no_results);
                }
            }

            @Override
            public void onFailure(Call<RecipeJTO> call, Throwable t) {
                Log.d(TAG, "call to food2track failed", t);
                relativeLayoutWithProgressBar.setVisibility(View.GONE);
                informationTextView.setVisibility(View.VISIBLE);
                informationTextView.setText(R.string.error_occur);
            }
        });
    }

    private void loadDataFromRequest(RecipeJTO body) {
        Recipe recipe = body.getRecipe();
        new DownloadImageTask(imageView).execute(recipe.getImageUrl());
        recipeTitleTextView.setText(recipe.getTitle());
        food2ForkUrlTextView.setText(recipe.getF2FUrl());
        publisherTextView.setText(recipe.getPublisher());
        socialRankTextView.setText(String.format("%.2f", recipe.getSocialRank()));
        publisherUrlTextView.setText(recipe.getPublisherUrl());
        sourceUrlTextView.setText(recipe.getSourceUrl());
        ArrayAdapter ad = new ArrayAdapter(getApplicationContext(),
                R.layout.recipe_ingredients_list_item, recipe.getIngredients());
        listView.setAdapter(ad);
    }

    private void getDataFromAnotherActivity() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String recipeIdFromBundle = extras.getString(RECIPE_ID);
            if (recipeIdFromBundle != null) {
                recipeId = recipeIdFromBundle;
            }
        }
    }

    @Override
    public int getLayoutFieldId() {
        return R.layout.recipe_activity;
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
