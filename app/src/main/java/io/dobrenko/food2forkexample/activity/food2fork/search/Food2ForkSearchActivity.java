package io.dobrenko.food2forkexample.activity.food2fork.search;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;
import io.dobrenko.food2forkexample.R;
import io.dobrenko.food2forkexample.activity.BaseActivity;
import io.dobrenko.food2forkexample.activity.food2fork.api.Food2ForkApi;
import io.dobrenko.food2forkexample.activity.food2fork.api.Food2ForkApiBuilder;
import io.dobrenko.food2forkexample.activity.food2fork.api.SearchParameters;
import io.dobrenko.food2forkexample.activity.food2fork.api.SortTypes;
import io.dobrenko.food2forkexample.activity.food2fork.recipe.Food2ForkRecipeActivity;
import io.dobrenko.food2forkexample.model.food2fork.jto.response.SearchJTO;
import io.dobrenko.food2forkexample.model.food2fork.jto.response.SearchRecipe;
import io.dobrenko.food2forkexample.network.PhoneConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static io.dobrenko.food2forkexample.activity.food2fork.recipe.Food2ForkRecipeActivity.RECIPE_ID;

public class Food2ForkSearchActivity extends BaseActivity implements RecipesRecyclerViewAdapter.AdapterCallback {

    private final String TAG = "Food2ForkSearchActivity";
    private Food2ForkApi food2forkApi;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.search_recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.search_filters_layout)
    LinearLayout filterLayout;
    @BindView(R.id.relative_layout_progress_bar)
    RelativeLayout relativeLayoutWithProgressBar;
    @BindView(R.id.search_page_editText)
    EditText pageEditText;
    @BindView(R.id.search_query_editText)
    EditText queryEditText;
    @BindView(R.id.search_sort_spinner)
    Spinner sortSpinner;
    @BindView(R.id.search_information_textView)
    TextView informationTextView;

    private RecipesRecyclerViewAdapter recipesRecyclerViewAdapter;
    private List<SearchRecipe> recipesList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        food2forkApi = Food2ForkApiBuilder.createFood2ForkClient();
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recipesRecyclerViewAdapter = new RecipesRecyclerViewAdapter();
        recipesRecyclerViewAdapter.setRecipeList(recipesList);
        recyclerView.setAdapter(recipesRecyclerViewAdapter);


    }

    @Override
    public int getLayoutFieldId() {
        return R.layout.search_activity;
    }


    @OnClick(R.id.search_find_button)
    public void searchRecipes() {
        String page = pageEditText.getText().toString();
        String query = queryEditText.getText().toString();
        String sort = sortSpinner.getSelectedItem().toString();
        searchRecipes(page, query, sort);
    }

    public void searchRecipes(String page, String query, String sort) {
        if (!PhoneConnection.isDeviceOnline(this)) {
            showToast("There is no internet connection");
            return;
        }
        informationTextView.setVisibility(View.GONE);
        recyclerView.setVisibility(View.GONE);
        relativeLayoutWithProgressBar.setVisibility(View.VISIBLE);
        Map<String, String> queryMap = buildQuery(page, query, sort);
        Call<SearchJTO> call = food2forkApi.search(queryMap);
        call.enqueue(new Callback<SearchJTO>() {
            @Override
            public void onResponse(Call<SearchJTO> call, Response<SearchJTO> response) {
                SearchJTO body = response.body();
                recipesList.clear();
                if (body != null) {
                    recipesList.addAll(body.getRecipes());
                    recipesRecyclerViewAdapter.notifyDataSetChanged();
                    recyclerView.setVisibility(View.VISIBLE);
                } else {
                    informationTextView.setVisibility(View.VISIBLE);
                    informationTextView.setText(R.string.information_no_results);
                }
                relativeLayoutWithProgressBar.setVisibility(View.GONE);

            }

            @Override
            public void onFailure(Call<SearchJTO> call, Throwable t) {
                Log.d(TAG, "call to food2track failed", t);
                relativeLayoutWithProgressBar.setVisibility(View.GONE);
                informationTextView.setVisibility(View.VISIBLE);
                informationTextView.setText(R.string.error_occur);
            }
        });
    }

    private Map<String, String> buildQuery(String page, String query, String sort) {
        HashMap<String, String> queryMap = new HashMap<>();
        queryMap.put(SearchParameters.QUERY.toString(), query);
        queryMap.put(SearchParameters.PAGE.toString(), page);
        String[] spinnerValues = getResources().getStringArray(R.array.search_sort_array);
        if (sort.equals(spinnerValues[0])) {
            queryMap.put(SearchParameters.SORT.toString(), SortTypes.best);
        } else {
            queryMap.put(SearchParameters.SORT.toString(), SortTypes.hottest);
        }
        return queryMap;
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


    @OnClick(R.id.search_filters_show_button)
    protected void onFiltersButtonPressed() {
        if (filterLayout.getVisibility() == View.GONE) {
            filterLayout.setVisibility(View.VISIBLE);
        } else {
            filterLayout.setVisibility(View.GONE);
        }
    }


    @Override
    public void onShowMoreButtonClick(String recipeId) {
        startRecipeActivity(recipeId);
    }

    private void startRecipeActivity(String recipeId) {
        Intent intent = new Intent(this, Food2ForkRecipeActivity.class);
        intent.putExtra(RECIPE_ID, recipeId);
        startActivity(intent);
    }


    @OnClick(R.id.search_section_next_page_button)
    public void searchNextPage() {
        Editable text = pageEditText.getText();
        int pageNumber = 1;
        try {
            pageNumber = Integer.parseInt(text.toString());
        } catch (NumberFormatException exception) {
            Log.d(TAG, "page value from edittext is not a number", exception.getCause());
        }
        if (recipesList.size() > 0) {
            int nextPage = pageNumber + 1;
            pageEditText.setText(Integer.toString(nextPage));
            searchRecipes();
        } else {
            showToast("Can't go to next page - there isn't more pages");
        }

    }

    @OnClick(R.id.search_section_prev_page_button)
    public void searchPrevPage() {
        Editable text = pageEditText.getText();
        int pageNumber = 1;
        try {
            pageNumber = Integer.parseInt(text.toString());
        } catch (NumberFormatException exeception) {
            Log.d(TAG, "page value from editText is not a number", exeception.getCause());
        }
        if (pageNumber > 1) {
            int prevPage = pageNumber - 1;
            pageEditText.setText(Integer.toString(prevPage));
            searchRecipes();
        } else {
            showToast("Can't go to previous page - it's a first page");
        }

    }
}
