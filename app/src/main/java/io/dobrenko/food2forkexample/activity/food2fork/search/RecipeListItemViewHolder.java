package io.dobrenko.food2forkexample.activity.food2fork.search;


import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.dobrenko.food2forkexample.R;

public class RecipeListItemViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.search_recycler_view_item_title)
    protected TextView itemTitleTextView;
    @BindView(R.id.search_recycler_view_item_rank)
    protected TextView itemRankTextView;
    @BindView(R.id.search_recycler_view_item_show_more)
    protected Button itemShowMoreButton;

    public RecipeListItemViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public TextView getItemTitleTextView() {
        return itemTitleTextView;
    }

    public TextView getItemRankTextView() {
        return itemRankTextView;
    }

    public TextView getItemShowMoreButton() {
        return itemShowMoreButton;
    }
}
